# pdfmerge

Merge multiple PDFs in your terminal into one big PDF. This program needs *pdflatex* (and the package *pdfpages*) to be installed.

## Installation

You can make the program available in your shell by adding this to your *.bashrc*:

    $ source ~/path_to_file/pdfmerge

Make sure the file is executable with:

    $ chmod +x pdfmerge

## Usage

To see every option that can be given take a look at the help function:

    $ pdfmerge -h

By default, the whole PDF will be merged. To only include single pages or sections, type them afterwards.
A sophisticated example would be:

    $ pdfmerge -v -l -n 1x2 file1.pdf 1-4,{} file2.pdf 7 file3.pdf 5,13-last

This will print debugging output, and will produce a landscape PDF with two pages next to each other. Page 1 to 4 from file1.pdf will be included, following a blank page, page 7 from file2.pdf, page 5 from file3.pdf, and all pages from page 13 to the last page from file3.pdf.
