#!/usr/bin/env bash
#
# Merge PDF files using pdflatex with the pdfpages package.

pdfmerge() {
    unset OPTIND OPTARG
    local commands=''
    local landscape=false
    local nup='1x1'
    local out='merged'
    local verbose=false

    # Handle input arguments
    while getopts :c:hln:o:v opt; do
        case $opt in
            c)  # Add pdfpages commands
                echo 'Warning: Invalid arguments for -c can break the program'
                commands=$OPTARG
                ;;
            h)  # Display help and exit
                pdfmerge_help
                return 0
                ;;
            l)  # Generate a landscape PDF
                landscape=true
                ;;
            n)  # Arguments for the pdfpages nup option
                nup=$OPTARG
                ;;
            o)  # Specify the output filename
                out=$OPTARG
                out=${out%.*}
                ;;
            v)  # Enable output from pdflatex for debugging
                verbose=true
                ;;
            \?) # Error handling
                echo 'Error: Invalid option' >&2
                return 1
                ;;
        esac
    done
    shift $((OPTIND-1))

    # Check if pdflatex is available
    if [[ ! -x $(command -v pdflatex) ]]; then
        echo 'Error: You have to install pdflatex' >&2
        return 1
    fi

    # Check if files are given
    if [[ "$#" = 0 ]]; then
        echo -e 'Error: No files are given\n' >&2
        pdfmerge_help
        return 1
    fi

    # Check for same name as output name
    for i in "$@"; do
        if [[ "${i%.*}" == "$out" ]]; then
            echo "Error: $i has the same name as the output" >&2
            return 1
        fi
    done

    # Create a temporary tex file
    tmp_tex=$(mktemp)
    trap 'rm "$tmp_tex"' EXIT SIGINT SIGTERM

    # Write parameters to file
    echo '\documentclass{article}' > "$tmp_tex"
    {
        echo '\usepackage{pdfpages}'
        echo '\begin{document}'
        echo -n '\includepdfmerge['
        echo -n "$commands"
    } >> "$tmp_tex"

    # Set landscape mode if specified
    if $landscape; then
        echo -n ',landscape' >> "$tmp_tex"
    fi

    # Add nup option if specified and well-formed
    if [[ "$nup" =~ ^[[:digit:]]+x[[:digit:]]+$ ]]; then
        echo -n ",nup=$nup" >> "$tmp_tex"
    else
        echo 'Warning: Argument for -n is malformed and will be skipped'
    fi

    echo -n ']{' >> "$tmp_tex"

    # First argument has to be a file
    if [[ -f "$1" ]]; then
        echo -n "$1" >> "$tmp_tex"
        shift
    else
        echo "Error: $1 can not be accessed" >&2
        return 1
    fi

    # Add files and page number identifiers
    local was_file=true
    for i in "$@"; do
        if [[ -f "$i" ]]; then
            # Add all pages if previous argument was a file as well
            if $was_file; then
                echo -n ",-" >> "$tmp_tex"
            fi
            # Add current file
            echo -n ",$i" >> "$tmp_tex"
            was_file=true
        else
            if $was_file; then
                # Add page options, only check for characters not syntax
                if [[ "$i" =~ ^[-,\{\}[:space:][:digit:]]+$ ]]; then
                    echo -n ",$i" >> "$tmp_tex"
                    was_file=false
                else
                    # Skip argument if malformed
                    echo "Warning: $i is not a file nor a page identifier and \
will be skipped"
                fi
            else
                # Abort if previous and current arguments are no files
                echo "Error: $i can not be accessed" >&2
                return 1
            fi
        fi
    done

    # If last file has no page identifier add all pages
    if $was_file; then
        echo -n ",-" >> "$tmp_tex"
    fi

    echo '}' >> "$tmp_tex"
    echo '\end{document}' >> "$tmp_tex"

    # Check if there are files to merge
    if [[ $(grep -c 'includepdf' "$tmp_tex") = 0 ]]; then
        echo 'Error: No files to merge' >&2
        rm "$tmp_tex"
        return 1
    fi

    # Create the (relative) output folder if it does not exists
    if [[ ! -d $(dirname "$out") ]]; then
        mkdir "$(dirname "$out")"
    fi

    # Merge PDF files with pdflatex, show output if verbose was used
    if $verbose; then
        pdflatex -halt-on-error -jobname="$out" "$tmp_tex" >&2
    else
        pdflatex -halt-on-error -jobname="$out" "$tmp_tex" > /dev/null
    fi

    # Remove log files
    rm "$out".aux
    rm "$out".log

    # Inform for success
    if [[ -f "$out".pdf ]]; then
        echo "Info: PDF files merged into $out.pdf"
    else
        echo 'Error: Something went wrong' >&2
        return 1
    fi
}

pdfmerge_help() {
    echo 'Usage: pdfmerge [OPTION]... FILE...'
    echo 'Merge PDF files using pdflatex with the pdfpages package.'
    echo 'This program can handle PDF, PNG, JPEG, and EPS files.'
    echo ''
    echo '  -c    add pdfpages commands, if a space separation is needed'
    echo '        guard the commands with " or '\'''
    echo '  -h    display help and exit'
    echo '  -l    generate a landscape PDF'
    echo '  -n    arguments for the pdfpages nup option, default is "1x1"'
    echo '  -o    specify the output filename, can not match an input filename'
    echo '  -v    enable output from pdflatex for debugging'
    echo ''
    echo 'Source code: https://gitlab.com/wangenau/pdfmerge/'
    echo 'pdfpages documentation: https://ctan.org/pkg/pdfpages/'
}
